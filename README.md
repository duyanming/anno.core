# Anno.Core

#### 介绍

Anno 是一个微服务快速开发框架，底层通讯可以随意切换 grpc、thrift。自带服务发现、调用链追踪、Cron 调度、限流、事件总线、CQRS 、DDD、类似MVC的开发体验，插件化开发。 Anno is a microservices rapid development framework, the underlying communication can be arbitrary switch GRPC, thrift.Built-in service discovery, call chain tracking, Cron scheduling, current limiting, event bus, CQRS, DDD, similar MVC development experience, plug-in development.

#### 项目地址

[dotnetchina](https://gitee.com/dotnetchina/anno.core) : https://gitee.com/dotnetchina/anno.core

[github](https://github.com/duyanming/Anno.Core) : https://github.com/duyanming/Anno.Core

#### 体验地址

[Viper](http://anno.liqingxi.cn/) :http://anno.liqingxi.cn/

#### 文档地址

[github](https://duyanming.github.io/) : https://duyanming.github.io/